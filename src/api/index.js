import axios from 'axios'
import { promises } from 'fs';
import { get } from 'http';

const baseURL = 'http://www.lovegf.cn:8899/api'
//设置当前代码的根域名
axios.defaults.baseURL = baseURL


//获取图片
export const getlunbo = () => {
  return axios.get('/getlunbo').then(res => res.data)
}
//获取新闻资讯
export const getNews = () => {
  return axios.get('/getnewslist').then(res => res.data)
}
//获取新闻资讯详情
export const getNewsInfo = params => {
  return axios.get(`getnew/${params}`).then(res => res.data)
}
//获取评论信息
export const getcomment = params => {
  return axios.get(`getcomments/${params.id}?pageindex=${params.pageindex}`).then(res => res.data)
}
//提交评论信息
export const postComment = params => {
  return axios.post(`postcomment/${params.id}`).then(res => res.data)
}
//获取图片分类数据
export const getImgcategory = params => {
  return axios.get(`getimgcategory`).then(res => res.data)
}