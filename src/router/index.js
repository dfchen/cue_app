import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/tabbar/Home'
import member from '@/components/tabbar/member'
import shopcar from '@/components/tabbar/shopcar'
import search from '@/components/tabbar/search'
import newslist from '@/components/news/NewsList'
import newsinfo from '@/components/news/Newsinfo'
import photolist from '@/components/photos/photoList'

Vue.use(Router)

var router = new Router({
  mode:'history',
  linkActiveClass:'mui-active',
  routes: [
    {
      path:'/home',
      name:'Home',
      component:Home
     
    },{
      path:'/member',
      name:'member',
      component:member
    },{
      path:'/shopcar',
      name:'shopcar',
      component:shopcar
    },{
      path:'/search',
      name:'search',
      component:search
    },{   
      path:'/home/newslist',
      name:'NewsList',
      component:newslist,
    },{
      path:'/home/newsinfo/:id',
      name:'NewsInfo',
      component:newsinfo
    },{
      path:'/photos/photoList',
      name:'photoList',
      component:photolist,
    }
  ]
})


export default router